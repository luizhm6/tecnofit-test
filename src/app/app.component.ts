import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass']
})
export class AppComponent {
	constructor(
		public translate: TranslateService
	) {
		translate.addLangs(['en', 'pt-br']);
		// this language will be used as a fallback when a translation isn't found in the current language
		translate.setDefaultLang('pt-br');

		// the lang to use, if the lang isn't available, it will use the current loader to get them
		translate.use('en');
		const browserLang = translate.getBrowserLang();
		translate.use(browserLang.match(/en|pt-br/) ? browserLang : 'pt-br');
	}
	title = 'angular';
}
