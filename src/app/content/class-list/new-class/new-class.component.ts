import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ClassListService } from '../class-list.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-new-class',
	templateUrl: './new-class.component.html',
	styleUrls: ['./new-class.component.scss']
})
export class NewClassComponent implements OnInit{
	ngOnInit(): void {
		console.log(this.id)
		if(this.id){
			this.classListService.getClass(this.id).then(
				res=>{
					this.classForm.patchValue({
						name: res.data[0].name,
						modality: res.data[0].modality,
						place: res.data[0].place,
						vacancies: res.data[0].vacancies,
						duration: res.data[0].duration,
						events: res.data[0].events,
						recurrence: res.data[0].recurrence,
						active: res.data[0].active
					})

				}
			)
		}
	}
	id: any;

	classForm = this.fb.group({
		name: [null, Validators.compose([
			Validators.required, Validators.minLength(3), Validators.maxLength(80)])
		],
		modality: [null, Validators.compose([
			Validators.required, Validators.minLength(3), Validators.maxLength(40)])
		],
		place: [null, Validators.compose([
			Validators.required, Validators.minLength(3), Validators.maxLength(40)])
		],
		vacancies: [null, Validators.required],
		duration: [null, Validators.required],
		events: [null, Validators.required],
		recurrence: [null, Validators.required],
		active: [null]
	});

	constructor(
		private route: ActivatedRoute,
		private fb: FormBuilder,
		private classListService: ClassListService
		) { 
			this.route.params.subscribe(
				params => {
					this.id = params['id']
					console.log(this.id)
				}
			);
		}

	onSubmit() {
		if(this.id){
			this.classForm.value.active = this.classForm.value.active ? 1 : 0;
			this.classForm.valid 
			? this.classListService.editClass(this.id, this.classForm.value) 
			: alert("preencha todos os campos obrigatorios")
		}else{
			this.classForm.valid 
			? this.classListService.saveNewClass(this.classForm.value) 
			: alert("preencha todos os campos obrigatorios")
		}
	}

}
