import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ClassListDataSource } from './class-list-datasource';
import { ClassListService } from './class-list.service';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-class-list',
	templateUrl: './class-list.component.html',
	styleUrls: ['./class-list.component.scss']
})
export class ClassListComponent implements OnInit, OnDestroy {
	// @ViewChild(MatPaginator) paginator: MatPaginator;
	// @ViewChild(MatSort) sort: MatSort;
	classListSub: Subscription
	list: any;
	temp: any;
	param = { value: 'world' };

	constructor(
		private classListService: ClassListService,
		translate: TranslateService
	) {
		translate.addLangs(['en', 'pt-br']);
		// this language will be used as a fallback when a translation isn't found in the current language
		translate.setDefaultLang('pt-br');

		// the lang to use, if the lang isn't available, it will use the current loader to get them
		translate.use('en');
		const browserLang = translate.getBrowserLang();
		translate.use(browserLang.match(/en|pt-br/) ? browserLang : 'pt-br');
	}


	/** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
	displayedColumns = ['name', 'modality', 'place', 'recurrence', 'vacancies', 'duration', 'options'];

	ngOnInit() {
		this.listClasses();
		this.classListSub = this.classListService.classListEmitter.subscribe(
			res => {
				this.list = res.data;
				this.temp = res.data;
			}
		);


	}

	listClasses() {
		this.classListService.getClassList();
	}

	deletItem(id) {
		this.classListService.deletClass(id)
	}

	updateFilter(event) {
		const val = event.target.value.toLowerCase();

		// filter our data
		const temp = this.temp.filter(function (d) {
			return d.name.toLowerCase().indexOf(val) !== -1 || !val;
		});

		// update the rows
		this.list = temp;

	}

	ngOnDestroy() {
		this.classListSub.unsubscribe();
	}

}
