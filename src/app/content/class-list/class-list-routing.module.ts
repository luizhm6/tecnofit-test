import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassListComponent } from './class-list.component';
import { NewClassComponent } from './new-class/new-class.component';

const classListRoutes: Routes = [
	{path: "", component: ClassListComponent},
	{path: "new-class", component: NewClassComponent},
	{path: ":id", component: NewClassComponent},
];

@NgModule({
  imports: [RouterModule.forChild(classListRoutes)],
  exports: [RouterModule]
})
export class ClassListRoutingModule { }
