import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http'
import { Router } from '@angular/router';
@Injectable({
	providedIn: 'root'
})
export class ClassListService {

	api: string = environment.api + '/class';
	classListEmitter = new EventEmitter;

	constructor(
		private http: Http,
		private router: Router
	) { }

	getClassList() {
		this.http.get(`${this.api}`)
			.subscribe(
				res => {
					return this.classListEmitter.emit(res.json());
				}
			)
	}

	getClass(id) {
		return this.http.get(`${this.api}/get/${id}`)
			.toPromise()
			.then(
				res => {
					console.log(res.json())
					return res.json();
				}
			)
			.catch(
				err => {

				}
			)
	}

	saveNewClass(body) {
		this.http.post(`${this.api}/save`, body)
			.toPromise()
			.then(
				res => {
					this.router.navigateByUrl('/');
					return res;
				}
			)
			.catch(
				err => {

				}
			)
	}

	editClass(id, body) {
		return this.http.post(`${this.api}/save/${id}`, body)
			.toPromise()
			.then(
				res => {
					this.router.navigateByUrl('/');
					console.log(res.json())
				}
			)
			.catch()
	}

	deletClass(id) {
		return this.http.delete(`${this.api}/save/${id}`)
			.toPromise()
			.then(
				res => {
					console.log(res.json())
				}
			)
			.catch()
	}
}
