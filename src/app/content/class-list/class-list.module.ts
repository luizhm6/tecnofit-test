import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassListComponent } from './class-list.component';
import { ClassListRoutingModule } from './class-list-routing.module';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatButtonModule, MatInputModule, MatRippleModule, MatIconModule, MatMenuModule, MatSelectModule, MatRadioModule, MatCardModule, MatSlideToggleModule } from '@angular/material';
import { ClassListService } from './class-list.service';
import { HttpModule } from '@angular/http';
import { NewClassComponent } from './new-class/new-class.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http);
}

@NgModule({
	declarations: [
		ClassListComponent,
		NewClassComponent
	],
	imports: [
		CommonModule,
		ClassListRoutingModule,
		MatTableModule,
		MatPaginatorModule,
		MatSortModule,
		HttpModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		MatRippleModule,
		MatIconModule,
		MatMenuModule,
		MatSelectModule,
		MatRadioModule,
		MatCardModule,
		ReactiveFormsModule,
		MatSlideToggleModule,
		HttpClientModule,
		TranslateModule.forChild()
	],
	providers: [
		ClassListService
	]
})

export class ClassListModule { }
